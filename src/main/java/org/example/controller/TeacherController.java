package org.example.controller;

import org.example.api.TeacherService;
import org.example.dto.StudentDTO;
import org.example.dto.TeacherDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("teachers")
public class TeacherController {

    @Autowired
    TeacherService teacherService;

    @PostMapping("save")
    public void saveTeacher(@RequestBody @Validated TeacherDTO teacherDTO) {
        teacherService.saveTeacher(teacherDTO);
    }

    @PostMapping(value = "delete/{id}")
    public void deleteTeacher(@PathVariable int id) {
        teacherService.deleteTeacher(id);
    }

    @GetMapping(value = "get/{id}")
    public TeacherDTO getTeacherById(@PathVariable int id) {
        return teacherService.getTeacherById(id);
    }

    @GetMapping("getAll")
    public List<TeacherDTO> getAllTeachers() {
        return teacherService.getAllTeachers();
    }

    @PostMapping(value = "{teacherId}/assignStudent/{studentId}")
    public void assignStudentToTeacher(@PathVariable int teacherId, @PathVariable int studentId) {
        teacherService.assignStudentToTeacher(studentId, teacherId);
    }

    @PostMapping(value = "{teacherId}/unsubscribeStudent/{studentId}")
    public void unsubscribeStudentFromTeacher(@PathVariable int teacherId, @PathVariable int studentId) {
        teacherService.unsubscribeStudentFromTeacher(studentId, teacherId);
    }

    @GetMapping(value = "{id}/getAllStudents")
    public List<StudentDTO> getAllStudentsOfTeacher(@PathVariable int id) {
        return teacherService.getAllStudentsOfTeacher(id);
    }
}
