package org.example.controller;

import org.example.api.StudentService;
import org.example.dto.StudentDTO;
import org.example.dto.TeacherDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestControllerAdvice
@RequestMapping("/students")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @PostMapping("save")
    public void saveStudent(@RequestBody @Validated StudentDTO studentDTO) {
        studentService.saveStudent(studentDTO);
    }

    @PostMapping(value = "delete/{id}")
    public void deleteStudent(@PathVariable int id) {
        studentService.deleteStudent(id);
    }

    @GetMapping(value = "get/{id}")
    public StudentDTO getStudentById(@PathVariable int id) {
        return studentService.getStudentById(id);
    }

    @GetMapping("getAll")
    public List<StudentDTO> getAllStudents() {
        return studentService.getAllStudents();
    }

    @GetMapping(value = "{id}/getAllTeachers")
    public List<TeacherDTO> getAllTeachersOfStudent(@PathVariable int id) {
        return studentService.getAllTeachersOfStudent(id);
    }
}
