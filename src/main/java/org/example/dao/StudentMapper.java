package org.example.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.example.model.StudentEntity;
import org.example.model.TeacherEntity;
import org.springframework.stereotype.Repository;

import java.util.List;


@Mapper
@Repository
public interface StudentMapper {
    @Insert("INSERT INTO students (first_name, last_name, age) " +
            "VALUES (#{firstName} ,#{lastName} ,#{age} )")
    void saveStudent(StudentEntity studentEntity);

    @Delete("DELETE FROM students WHERE id = #{id}")
    void deleteStudent(int id);

    @Select("SELECT * FROM students WHERE id = #{id}")
    StudentEntity getStudentById(int id);

    @Select("SELECT * FROM students")
    List<StudentEntity> getAllStudents();

    @Select("SELECT t.* FROM teachers t JOIN students_teachers st ON t.id = st.teacher_id " +
            "WHERE student_id = #{id}")
    List<TeacherEntity> getAllTeachersOfStudent(int id);
}
