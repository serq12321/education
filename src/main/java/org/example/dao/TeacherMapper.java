package org.example.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.example.model.StudentEntity;
import org.example.model.StudentTeacherEntity;
import org.example.model.TeacherEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface TeacherMapper {

    @Insert("INSERT INTO teachers(first_name, last_name, age, specialty)" +
            "values (#{firstName}, #{lastName}, #{age}, #{specialty})")
    void saveTeacher(TeacherEntity teacherEntity);

    @Delete("DELETE FROM teachers WHERE id = #{id}")
    void deleteTeacher(int id);

    @Select("SELECT * FROM teachers WHERE id = #{id}")
    TeacherEntity getTeachersById(int id);

    @Select("SELECT * FROM teachers")
    List<TeacherEntity> getAllTeachers();

    @Insert("INSERT INTO students_teachers (student_id, teacher_id)" +
            "VALUES (#{studentId}, #{teacherId})")
    void assignStudentToTeacher(StudentTeacherEntity studentTeacherEntity);

    @Delete("DELETE FROM students_teachers " +
            "WHERE student_id = #{studentId} AND teacher_id = #{teacherId}")
    void unsubscribeStudentFromTeacher(StudentTeacherEntity studentTeacherEntity);

    @Select("SELECT s.* FROM students AS s JOIN students_teachers AS st ON s.id = st.student_id " +
            "WHERE teacher_id = #{id}")
    List<StudentEntity> getAllStudentsOfTeacher(int id);

}
