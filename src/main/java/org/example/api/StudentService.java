package org.example.api;

import org.example.dto.StudentDTO;
import org.example.dto.TeacherDTO;

import java.util.List;

public interface StudentService {
    void saveStudent(StudentDTO studentDTO);

    void deleteStudent(int id);

    StudentDTO getStudentById(int id);

    List<StudentDTO> getAllStudents();

    List<TeacherDTO> getAllTeachersOfStudent(int id);
}
