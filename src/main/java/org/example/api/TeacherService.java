package org.example.api;

import org.example.dto.StudentDTO;
import org.example.dto.TeacherDTO;

import java.util.List;

public interface TeacherService {
    void saveTeacher(TeacherDTO teacherDTO);

    void deleteTeacher(int id);

    TeacherDTO getTeacherById(int id);

    List<TeacherDTO> getAllTeachers();

    void assignStudentToTeacher(int studentId, int teacherId);

    void unsubscribeStudentFromTeacher(int studentId, int teacherId);

    List<StudentDTO> getAllStudentsOfTeacher(int id);
}
