package org.example.service;

import org.example.api.StudentService;
import org.example.dao.StudentMapper;
import org.example.dto.StudentDTO;
import org.example.dto.TeacherDTO;
import org.example.model.StudentEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentMapper studentMapper;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    @Transactional
    public void saveStudent(StudentDTO studentDTO) {
        StudentEntity studentEntity = modelMapper.map(studentDTO, StudentEntity.class);
        studentMapper.saveStudent(studentEntity);
    }

    @Override
    public void deleteStudent(int id) {
        studentMapper.deleteStudent(id);
    }

    @Override
    public StudentDTO getStudentById(int id) {
        return modelMapper.map(studentMapper.getStudentById(id), StudentDTO.class);
    }

    @Override
    public List<StudentDTO> getAllStudents() {
        return studentMapper.getAllStudents().stream()
                .map(studentEntity -> modelMapper.map(studentEntity, StudentDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<TeacherDTO> getAllTeachersOfStudent(int id) {
        return studentMapper.getAllTeachersOfStudent(id).stream()
                .map(teachersEntity -> modelMapper.map(teachersEntity, TeacherDTO.class))
                .collect(Collectors.toList());
    }
}
