package org.example.service;

import org.example.api.TeacherService;
import org.example.dao.TeacherMapper;
import org.example.dto.StudentDTO;
import org.example.dto.TeacherDTO;
import org.example.model.StudentTeacherEntity;
import org.example.model.TeacherEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    TeacherMapper teacherMapper;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    DataSource dataSource;


    @Override
    @Transactional
    public void saveTeacher(TeacherDTO teacherDTO) {
        TeacherEntity teacherEntity = modelMapper.map(teacherDTO, TeacherEntity.class);
        teacherMapper.saveTeacher(teacherEntity);
    }

    @Override
    public void deleteTeacher(int id) {
        teacherMapper.deleteTeacher(id);
    }

    @Override
    public TeacherDTO getTeacherById(int id) {
        return modelMapper.map(teacherMapper.getTeachersById(id), TeacherDTO.class);
    }

    @Override
    public List<TeacherDTO> getAllTeachers() {
        return teacherMapper.getAllTeachers().stream()
                .map(teacherEntity -> modelMapper.map(teacherEntity, TeacherDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public void assignStudentToTeacher(int studentId, int teacherId) {
        teacherMapper.assignStudentToTeacher(new StudentTeacherEntity(studentId, teacherId));
    }

    @Override
    public void unsubscribeStudentFromTeacher(int studentId, int teacherId) {
        teacherMapper.unsubscribeStudentFromTeacher(new StudentTeacherEntity(studentId, teacherId));
    }

    @Override
    public List<StudentDTO> getAllStudentsOfTeacher(int id) {
        return teacherMapper.getAllStudentsOfTeacher(id).stream()
                .map(studentEntity -> modelMapper.map(studentEntity, StudentDTO.class))
                .collect(Collectors.toList());
    }
}
