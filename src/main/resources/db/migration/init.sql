CREATE TABLE students
(
    id         SERIAL,
    first_name VARCHAR(30),
    last_name  VARCHAR(30),
    age        INT,
    PRIMARY KEY (id)
);

CREATE TABLE teachers
(
    id         SERIAL,
    first_name VARCHAR(30),
    last_name  VARCHAR(30),
    age        INT,
    specialty  VARCHAR(50),
    PRIMARY KEY (id)
);

CREATE TABLE students_teachers
(
    student_id INT,
    teacher_id INT,
    PRIMARY KEY (student_id, teacher_id),
    FOREIGN KEY (student_id) references students (id),
    FOREIGN KEY (teacher_id) references teachers (id)
);